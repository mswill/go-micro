package main

import (
	"context"
	"fmt"
	"gitlab.com/mswill/logger-service/data"
	"gitlab.com/mswill/logger-service/routes"
	myRPC "gitlab.com/mswill/logger-service/rpc"
	"net/rpc"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"strings"
	"sync"
	"time"
)

var client *mongo.Client
var mongoURL = os.Getenv("MONGO_URL")
var mongoUser = os.Getenv("MONGO_USER")
var mongoPass = os.Getenv("MONGO_PASSWORD")
var dataBase = os.Getenv("MONGO_INITDB_DATABASE")

func main() {
	wg := &sync.WaitGroup{}
	//-
	mongoClient, err := connectToMongo()
	if err != nil {
		log.Panicln(err)
	}

	//-
	client = mongoClient

	//-
	// disconnect mongo when be need
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	//-
	defer func() {
		err := client.Disconnect(ctx)
		if err != nil {
			panic(err)
		}
	}()

	//-
	app := routes.Config{Models: data.NewMongo(client)}

	// Register new rpc server
	_ = rpc.Register(&myRPC.RPCServer{})

	//- GRPC SERVICE LISTENER
	wg.Add(1)
	go app.GRPCListener(wg)

	//- RPC SERVICE LISTENER
	wg.Add(1)
	go func() {
		_ = app.RPCListener(wg)
	}()

	//- LOGGER SERVER
	wg.Add(1)
	go app.Serve(wg)

	wg.Wait()
}

func connectToMongo() (*mongo.Client, error) {

	//-
	clientOptions := options.Client().ApplyURI(mongoURL + dataBase)
	//clientOptions.SetAuth(options.Credential{
	//	Username: mongoUser,
	//	Password: mongoPass,
	//})

	//-
	// connect
	connect, err := mongo.Connect(context.Background(), clientOptions)
	if err != nil {
		log.Fatalln("Error connection to MONGO")
	}

	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting MONGODB on port: "+mongoURL, strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	//-
	return connect, nil
}
