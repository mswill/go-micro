package routes

import (
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/mswill/logger-service/data"
	logs "gitlab.com/mswill/logger-service/generated_proto"
	"gitlab.com/mswill/logger-service/grpcLogs"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"os"
	"strings"
	"sync"
)

var loggerServerPort = os.Getenv("LOGGER_SERVER_PORT")
var grpcLoggerServerPort = os.Getenv("LOGGER_GRPC_PORT")

type Config struct {
	Models data.Models
}

func (app Config) Routes() http.Handler {
	mux := chi.NewRouter()

	// specify who is allowed to connect
	mux.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))

	mux.Use(middleware.Heartbeat("/ping"))
	mux.Post("/log", app.WriteLog)
	//-

	return mux
}

// Serve START SERVER
func (app Config) Serve(wg *sync.WaitGroup) {
	defer wg.Done()

	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting logger-Service on port: "+os.Getenv("LOGGER_SERVER_PORT"), strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//-
	srv := &http.Server{
		Addr:    ":" + loggerServerPort,
		Handler: app.Routes(),
	}

	//-
	err := srv.ListenAndServe()
	if err != nil {
		log.Panic(err)
	}

}

// RPCListener START RPC LISTENER
func (app *Config) RPCListener(wg *sync.WaitGroup) error {
	defer wg.Done()
	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting RPC Server on port: "+os.Getenv("LOGGER_RPC_PORT"), strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//-
	rpcServerURL := "0.0.0.0:" + os.Getenv("LOGGER_RPC_PORT")
	listen, err := net.Listen("tcp", rpcServerURL)
	if err != nil {
		fmt.Println("ERROR: RPC Listener")
		return err
	}
	defer listen.Close()
	//-

	for {
		rpcConn, err := listen.Accept()
		if err != nil {
			fmt.Println("ERROR: RPC listen Accept")
			continue
		}

		go rpc.ServeConn(rpcConn)
	}
}

func (app *Config) GRPCListener(wg *sync.WaitGroup) {
	defer wg.Done()

	//-
	grpcLoggerServerPortURL := "0.0.0.0:" + grpcLoggerServerPort
	listen, err := net.Listen("tcp", grpcLoggerServerPortURL)
	if err != nil {
		log.Fatalf("FATAL: to GRPC Listen TCP: %s ", err)
	}

	//-
	// Create new grpc server
	s := grpc.NewServer()

	//-
	logs.RegisterLogServiceServer(s,
		&grpcLogs.LogServer{
			Model: app.Models,
		},
	)

	//-
	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting GRPC Server on port: "+grpcLoggerServerPort, strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//- start GRPC Server
	err = s.Serve(listen)
	if err != nil {
		log.Fatalf("FATAL: to start GRPC SERVER: %s", err)
		return
	}
}
