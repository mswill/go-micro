package routes

import (
	"fmt"
	"gitlab.com/mswill/logger-service/data"
	"net/http"
)

type JSONPayload struct {
	Name string `json:"name"`
	Data string `json:"data"`
}

func (app Config) WriteLog(writer http.ResponseWriter, request *http.Request) {

	//-
	var reqPayload JSONPayload
	_ = app.ReadJSON(writer, request, &reqPayload)

	//-
	// insert data
	event := data.LogEntry{
		Name: reqPayload.Name,
		Data: reqPayload.Data,
	}
	//-
	err := app.Models.LogEntry.Insert(event)
	if err != nil {
		fmt.Println("logger/handlers: 32")
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	res := jsonResponse{
		Error:   false,
		Message: "Logged",
	}

	//-
	_ = app.WriteJSON(writer, http.StatusAccepted, res)
}
