FROM golang:1.18.0-alpine as builder
RUN echo '----------------- LOGGER-SERVICE-DEV -----------------'

# deploy
RUN mkdir /app
WORKDIR /app

RUN apk add --update make git vim curl gcc
RUN curl -sSfL https://raw.githubusercontent.com/cosmtrek/air/master/install.sh | sh -s -- -b $(go env GOPATH)/bin

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY ./ ./

#CMD ["go", "run", "/app/cmd/web/main.go"]
CMD ["air"]
