FROM golang:1.18.0-alpine as builder
RUN echo '----------------- LOGGER-SERVICE -----------------'

# deploy
RUN mkdir /app
WORKDIR /app

COPY go.mod ./
#COPY go.sum ./
RUN go mod download

COPY ./ ./
RUN CGO_ENABLED=0 GOOS=linux go build -o loggerApp ./cmd/logger/

RUN chmod  +x /app/loggerApp

# build
FROM alpine:3.11.3
RUN mkdir /app
COPY --from=builder /app/loggerApp /app
CMD ["/app/loggerApp"]
