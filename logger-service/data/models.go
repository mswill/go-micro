package data

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"gopkg.in/mgo.v2/bson"
	"time"
)

var Client *mongo.Client

type Models struct {
	LogEntry LogEntry
}

type LogEntry struct {
	ID        string    `json:"id,omitempty" bson:"_id,omitempty"`
	Name      string    `json:"name,omitempty" bson:"name"`
	Data      string    `json:"data,omitempty" bson:"data"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}

func NewMongo(mongo *mongo.Client) Models {
	Client = mongo

	return Models{
		LogEntry: LogEntry{},
	}
}

func (l *LogEntry) Insert(entry LogEntry) error {
	//-
	collection := Client.Database("logs").Collection("logs")
	//-
	_, err := collection.InsertOne(context.Background(), LogEntry{
		Name:      entry.Name,
		Data:      entry.Data,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	})
	if err != nil {
		fmt.Printf("%s\n", "Error INSERT TO MONGO")
		return err
	}

	//-
	return nil
}

func (l *LogEntry) GetAll() ([]*LogEntry, error) {
	//-
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	//-
	collection := Client.Database("logs").Collection("logs")

	//-
	ops := options.Find()
	ops.SetSort(bson.D{{"created_at", -1}})
	//-
	cursor, err := collection.Find(ctx, bson.D{}, ops)
	if err != nil {
		fmt.Printf("%s\n", "Error FIND ALL DOCS MONGO")
		return nil, err
	}

	//-
	defer cursor.Close(ctx)

	//-
	var logs []*LogEntry

	for cursor.Next(ctx) {
		var item LogEntry
		err := cursor.Decode(&item)
		if err != nil {
			fmt.Printf("%s %s\n", "Error decoding log into slice ", err.Error())
			return nil, err
		} else {
			logs = append(logs, &item)
		}
	}

	//- return
	return logs, err
}

func (l LogEntry) GetOne(id string) (*LogEntry, error) {
	//-
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	//-
	collection := Client.Database("logs").Collection("logs")

	//-
	//- string to HEX
	docID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		fmt.Printf("Error to convert STRING to HEX ", err.Error())
		return nil, err
	}

	//-
	var entry LogEntry
	err = collection.FindOne(ctx, bson.M{"_id": docID}).Decode(&entry)
	if err != nil {
		fmt.Printf("Error: Decode to entry")
		return nil, err
	}

	//- return
	return &entry, nil
}

func (l LogEntry) DropCollection() error {
	//-
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	//-
	collection := Client.Database("logs").Collection("logs")
	err := collection.Drop(ctx)
	if err != nil {
		fmt.Printf("Error: DROP Collection %s", err.Error())
		return err
	}

	//- return
	return nil
}

func (l *LogEntry) UpdateDoc() (*mongo.UpdateResult, error) {
	//-
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	//-
	collection := Client.Database("logs").Collection("logs")

	//-
	//- string to HEX
	docID, err := primitive.ObjectIDFromHex(l.ID)
	if err != nil {
		fmt.Printf("Error to convert STRING to HEX ", err.Error())
		return nil, err
	}

	//-
	one, err := collection.UpdateOne(ctx,
		bson.M{"_id": docID},
		bson.D{
			{"$set", bson.D{
				{"name", l.Name},
				{"data", l.Data},
				{"updated_at", time.Now()},
			}},
		},
	)
	if err != nil {
		fmt.Printf("Error: Update Document %s", err.Error())
		return nil, err
	}

	//- result
	return one, nil
}
