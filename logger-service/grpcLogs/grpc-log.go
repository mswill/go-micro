package grpcLogs

import (
	"context"
	"fmt"
	"gitlab.com/mswill/logger-service/data"
	logs "gitlab.com/mswill/logger-service/generated_proto"
)

// LogServer Implementation
type LogServer struct {
	logs.UnimplementedLogServiceServer
	Model data.Models
}

func (l *LogServer) WriteLog(ctx context.Context, req *logs.LogRequest) (*logs.LogResponse, error) {
	//-
	input := req.GetLogEntry()

	// write the log
	logEntry := data.LogEntry{
		Name: input.Name,
		Data: input.Data,
	}

	//-
	err := l.Model.LogEntry.Insert(logEntry)
	if err != nil {
		fmt.Println("ERROR: grpc Insert")
		return &logs.LogResponse{Result: "grpc failed to write"}, err
	}

	res := &logs.LogResponse{Result: "GRPC logged"}
	//- return
	return res, nil
}
