package rpc

import (
	"context"
	"fmt"
	"gitlab.com/mswill/logger-service/data"
	"time"
)

type RPCServer struct{}

type RPCPayload struct {
	Name string
	Data string
}

func (r *RPCServer) LogInfo(payload *RPCPayload, resp *string) error {
	//-
	collection := data.Client.Database("logs").Collection("logs")

	//-
	_, err := collection.InsertOne(context.Background(), data.LogEntry{
		Name:      payload.Name,
		Data:      payload.Data,
		CreatedAt: time.Now(),
	})
	//-
	if err != nil {
		fmt.Printf("ERROR: logger writting to mongo")
		return err
	}
	//-
	*resp = "Processed payload RPC: " + payload.Name

	//- return
	return nil
}
