package main

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"gitlab.com/mswill/broker-service/routes"
	"log"
	"math"
	"net/http"
	"os"
	"strings"
	"time"
)

const BROKER_SERVER_PORT = "11000"

func main() {

	//-
	// connect rabbitmq
	connect, err := rabbitMQConnect()
	if err != nil {
		log.Fatal(err)
	}
	defer connect.Close()

	//-
	app := routes.Config{
		RabbitMQ: connect,
	}

	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting Broker-Service on port: "+os.Getenv("BROKER_SERVER_PORT"), strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//-
	// define server
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", os.Getenv("BROKER_SERVER_PORT")),
		Handler: app.Routes(),
	}

	//-
	// start server
	err = srv.ListenAndServe()
	if err != nil {
		log.Panic(err)
	}

}
func rabbitMQConnect() (*amqp.Connection, error) {

	//-
	var counts int64
	var backOff = 1 * time.Second
	var connection *amqp.Connection

	//-
	//- don't continue until rabbit is ready

	for {
		conn, err := amqp.Dial("amqp://quest:quest@rabbitmq")
		if err != nil {
			fmt.Println("ERROR: RabbitMQ not yet ready...")
			counts++
		} else {
			connection = conn
			fmt.Println("Connected to RABBITMQ")
			fmt.Printf("%s\n", strings.Repeat("-", 25))
			fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting Listener-Service on port: "+os.Getenv("LISTENER_SERVER_PORT"), strings.Repeat("-", 10))
			fmt.Printf("%s\n", strings.Repeat("-", 25))
			break
		}

		//-
		if counts > 5 {
			fmt.Println("Counts great than 5 ", err)
			return nil, err
		}

		//-
		backOff = time.Duration(math.Pow(float64(counts), 2)) * time.Second
		fmt.Printf("backing off ...\n")
		time.Sleep(backOff)
		continue
	}

	//- return
	return connection, nil
}
