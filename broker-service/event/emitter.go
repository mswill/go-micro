package event

import (
	"context"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
)

type Emitter struct {
	connection *amqp.Connection
}

func (e *Emitter) setup() error {
	//-
	channel, err := e.connection.Channel()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer channel.Close()

	//- return
	return DeclareExchange(channel)
}
func (e Emitter) Push(event string, severity string) error {
	channel, err := e.connection.Channel()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer channel.Close()

	fmt.Println("Pushing to channel ")

	//-
	err = channel.PublishWithContext(
		context.Background(),
		"logs_topic",
		severity,
		false,
		false,
		amqp.Publishing{ContentType: "text/plain", Body: []byte(event)})
	if err != nil {
		fmt.Println(err)
		return err
	}
	//-

	//- return
	return nil
}

func NewEventEmitter(conn *amqp.Connection) (*Emitter, error) {
	emitter := &Emitter{
		connection: conn,
	}
	err := emitter.setup()
	if err != nil {
		fmt.Println(err)
		return &Emitter{}, err
	}

	//-
	return emitter, err
}
