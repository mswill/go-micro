package routes

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"gitlab.com/mswill/broker-service/event"
	genProto "gitlab.com/mswill/broker-service/generated_proto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net/http"
	"net/rpc"
	"os"
	"time"
)

//-
// Auth-Server HOST
var authServiceHost = os.Getenv("AUTH_SERVICE")
var loggerServiceHost = os.Getenv("LOGGER_SERVICE")
var loggerServiceHostGRPCPort = os.Getenv("LOGGER_SERVICE_GRPC_PORT")
var mailServiceHost = os.Getenv("MAIL_SERVICE")

// RequestPayload Request DATA
type RequestPayload struct {
	Action string      `json:"action"`
	Auth   AuthPayload `json:"auth,omitempty"`
	Log    LogPayload  `json:"log,omitempty"`
	Mail   MailPayload `json:"mail,omitempty"`
}

// AuthPayload FOR A AUTH-SERVICE
type AuthPayload struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// LogPayload FOR A LOGGER-SERVICE
type LogPayload struct {
	Name string `json:"name"`
	Data string `json:"data"`
}

// MailPayload FOR A MAIL-SERVICE
type MailPayload struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Subject string `json:"subject"`
	Message string `json:"message"`
}

// RPCPayload FOR A LOGGER-SERVICE FOR RPC SERVER
type RPCPayload struct {
	Name string
	Data string
}

// Handle Done!
func (app *Config) Handle(writer http.ResponseWriter, request *http.Request) {

	//-
	var requestPayload RequestPayload

	//-
	err := app.ReadJSON(writer, request, &requestPayload)
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	// check type of request
	fmt.Println("BROKER-SERVICE ACTION:  ", requestPayload.Action)
	switch requestPayload.Action {
	case "auth":
		app.auth(writer, &requestPayload.Auth)
	case "log":
		//app.log(writer, &requestPayload.Log)
		//app.logItemRPC(writer, &requestPayload.Log)
		app.LogEventRabbit(writer, &requestPayload.Log)
	case "mail":
		app.sendMail(writer, &requestPayload.Mail)
	default:
		_ = app.ErrorJSON(writer, errors.New("Unknown Action"))
	}
}

// Broker Done!
func (app *Config) Broker(w http.ResponseWriter, r *http.Request) {

	//-
	payload := jsonResponse{
		Error:   false,
		Message: "Hit the broker",
	}

	_ = app.WriteJSON(w, http.StatusOK, payload)
}

// Done!
func (app *Config) auth(w http.ResponseWriter, a *AuthPayload) {
	//-
	// create json
	jsonData, _ := json.MarshalIndent(a, "", "\t")

	//-
	// call the auth-service
	authServiceURL := "http://" + authServiceHost + ":12000" + "/auth"
	request, err := http.NewRequest("POST", authServiceURL, bytes.NewBuffer(jsonData))
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	client := &http.Client{}
	res, err := client.Do(request)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	defer res.Body.Close()

	//-
	// make sure get back our data
	if res.StatusCode == http.StatusUnauthorized {
		_ = app.ErrorJSON(w, errors.New("Invalid credentials"))
		return
	} else if res.StatusCode != http.StatusAccepted {
		_ = app.ErrorJSON(w, errors.New("Error calling auth-service"))
		return
	}

	//-
	// create response body
	var jsonFromService jsonResponse
	err = json.NewDecoder(res.Body).Decode(&jsonFromService)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	if jsonFromService.Error {
		_ = app.ErrorJSON(w, err, http.StatusUnauthorized)
		return
	}

	//-
	var payload jsonResponse
	payload.Error = false
	payload.Message = "Authenticated"
	payload.Data = jsonFromService.Data

	//-
	_ = app.WriteJSON(w, http.StatusAccepted, payload)
}

// Done!
func (app *Config) log(writer http.ResponseWriter, log *LogPayload) {
	fmt.Println("-------------- LOG ---------------")

	//-
	jsonData, _ := json.MarshalIndent(log, "", "\t")

	//-
	loggerServiceURL := "http://" + loggerServiceHost + ":13000" + "/log"
	request, err := http.NewRequest(http.MethodPost, loggerServiceURL, bytes.NewBuffer(jsonData))
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	request.Header.Set("Content-Type", "application/json")

	//-
	client := &http.Client{}
	res, err := client.Do(request)
	defer res.Body.Close()

	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	if res.StatusCode != http.StatusAccepted {
		_ = app.ErrorJSON(writer, err)
		return
	}
	//-
	var payLoad jsonResponse
	payLoad.Error = false
	payLoad.Message = "Logged"

	//-
	_ = app.WriteJSON(writer, http.StatusAccepted, payLoad)
}

// Done!
func (app *Config) sendMail(writer http.ResponseWriter, mail *MailPayload) {
	//-
	jsonData, _ := json.MarshalIndent(mail, "", "\t")

	//-
	mailServiceURL := "http://" + mailServiceHost + ":15000" + "/send"
	request, err := http.NewRequest(http.MethodPost, mailServiceURL, bytes.NewBuffer(jsonData))
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	request.Header.Set("Content-Type", "application/json")

	//-
	client := &http.Client{}
	res, err := client.Do(request)
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}
	defer res.Body.Close()

	if res.StatusCode != http.StatusAccepted {
		_ = app.ErrorJSON(writer, errors.New("ERROR: calling MAIL-SERVICE"))
		return
	}

	//-
	var payload jsonResponse
	payload.Error = false
	payload.Message = "Message sent to " + mail.To

	//-
	_ = app.WriteJSON(writer, http.StatusAccepted, payload)

	//-
	fmt.Println("Response ", res)
}

// Done!
func (app *Config) logItemRPC(w http.ResponseWriter, l *LogPayload) {

	//-
	loggerRPCServerURL := loggerServiceHost + ":5001"
	client, err := rpc.Dial("tcp", loggerRPCServerURL)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	rpcPayload := &RPCPayload{
		Name: l.Name,
		Data: l.Data,
	}

	//-
	// result will be filled AFTER calling rpc
	var result string

	//-
	// client calling
	err = client.Call("RPCServer.LogInfo", rpcPayload, &result)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	payload := jsonResponse{
		Error:   false,
		Message: result,
	}

	//-
	_ = app.WriteJSON(w, http.StatusAccepted, payload)
}

// LogItemGRPC Done!
func (app *Config) LogItemGRPC(w http.ResponseWriter, r *http.Request) {

	//-
	var requestPayload RequestPayload
	err := app.ReadJSON(w, r, &requestPayload)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-

	loggerServiceGrpcURL := loggerServiceHost + ":" + loggerServiceHostGRPCPort
	conn, err := grpc.Dial(loggerServiceGrpcURL,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
		grpc.WithBlock(),
	)
	defer conn.Close()
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	c := genProto.NewLogServiceClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	//-
	_, err = c.WriteLog(ctx, &genProto.LogRequest{
		LogEntry: &genProto.Log{
			Name: requestPayload.Log.Name,
			Data: requestPayload.Log.Data,
		},
	})
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	var payload jsonResponse
	payload.Error = false
	payload.Message = "grpc logged"

	//-
	_ = app.WriteJSON(w, http.StatusAccepted, payload)
}

//
func (app *Config) LogEventRabbit(w http.ResponseWriter, l *LogPayload) {
	err := app.pushToQueue(l.Name, l.Data)
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	var payload jsonResponse
	payload.Error = false
	payload.Message = "logged with RabbitMQ"

	//-
	_ = app.WriteJSON(w, http.StatusAccepted, &payload)
}

//
func (app *Config) pushToQueue(name, msg string) error {
	emitter, err := event.NewEventEmitter(app.RabbitMQ)
	if err != nil {
		fmt.Println(err)
		return err

	}

	//-
	payload := LogPayload{
		Name: name,
		Data: msg,
	}
	//-
	j, _ := json.MarshalIndent(&payload, "", "\t")
	err = emitter.Push(string(j), "log.INFO")
	if err != nil {
		fmt.Println(err)
		return err
	}

	//-
	return nil
}
