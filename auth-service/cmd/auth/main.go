package main

import (
	"database/sql"
	"fmt"
	_ "github.com/jackc/pgconn"
	_ "github.com/jackc/pgx/v4"
	_ "github.com/jackc/pgx/v4/stdlib"
	"gitlab.com/mswill/broker-service/data"
	"gitlab.com/mswill/broker-service/routes"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

var counts int64

func main() {
	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting Auth-Service on port: "+os.Getenv("AUTH_SERVER_PORT"), strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//-
	// Connect to DB
	conn := connectToDB()
	if conn == nil {
		log.Panic("Can't  connect to Postgres")
	}

	//-
	// Setup Config
	app := routes.Config{
		DB:     conn,
		Models: data.NewModel(conn),
	}

	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", os.Getenv("AUTH_SERVER_PORT")),
		Handler: app.Routes(),
	}

	//-
	err := srv.ListenAndServe()
	if err != nil {
		log.Fatalf(err.Error())
	}
}
func openDB(dsn string) (*sql.DB, error) {
	//-
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		log.Fatalf("%s\n", err.Error())
		return nil, err
	}

	//-
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	//-
	return db, err
}
func connectToDB() *sql.DB {
	//-
	dsn := os.Getenv("DSN")

	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), os.Getenv("DSN"), strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	//-
	for {
		connection, err := openDB(dsn)
		if err != nil {
			fmt.Printf("%s\n", "Postgres not yet to connected... ")
			counts++
		} else {
			fmt.Printf("%s\n", "Postgres to connect SUCCESS")
			return connection
		}

		//-
		if counts > 10 {
			fmt.Println(counts)
			log.Fatal(err)
		}

		//-
		fmt.Printf("%s\n", "Backing off for TWO SECONDS ")
		time.Sleep(time.Second * 2)
		continue
	}
}
