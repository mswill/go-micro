FROM golang:1.18.0-alpine as builder
RUN echo '----------------- AUTH-SERVICE -----------------'

# deploy
RUN mkdir /app
WORKDIR /app

COPY go.mod ./
#COPY go.sum ./
RUN go mod download

COPY ./ ./
RUN CGO_ENABLED=0 GOOS=linux go build -o authApp ./cmd/auth

RUN chmod  +x /app/authApp

# build
FROM alpine:3.11.3
RUN mkdir /app
COPY --from=builder /app/authApp /app
CMD ["/app/authApp"]
