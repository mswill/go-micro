package routes

import (
	"database/sql"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/mswill/broker-service/data"
	"net/http"
)

type Config struct {
	DB     *sql.DB
	Models data.Models
}

func (app *Config) Routes() http.Handler {
	mux := chi.NewRouter()

	//-
	// middleware
	mux.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"https://*", "http://*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300,
	}))
	mux.Use(middleware.Heartbeat("/ping"))

	//-
	mux.Post("/auth", app.Authenticate)

	//-
	return mux
}
