package routes

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"os"
)

var logServiceUrl = os.Getenv("LOGGER_SERVICE")

func (app *Config) Authenticate(w http.ResponseWriter, r *http.Request) {

	//-
	var requestPayload struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	//-
	err := app.ReadJSON(w, r, &requestPayload)
	if err != nil {
		_ = app.ErrorJSON(w, err, http.StatusBadRequest)
		return
	}

	//-
	// validate
	user, err := app.Models.User.GetByEmail(requestPayload.Email)
	if err != nil {
		_ = app.ErrorJSON(w, errors.New("invalid credentials"), http.StatusBadRequest)
		return
	}
	//-
	valid, err := user.PasswordMatches(requestPayload.Password)
	if err != nil || !valid {
		_ = app.ErrorJSON(w, errors.New("invalid credentials"), http.StatusBadRequest)
		return
	}

	//-
	// FOR LOGGER-SERVICE Authenticate
	err = app.logRequest("authenticated", fmt.Sprintf("%s", user.Email))
	if err != nil {
		_ = app.ErrorJSON(w, err)
		return
	}

	//-
	payload := jsonResponse{
		Error:   false,
		Message: fmt.Sprintf("Logged in user %s", user.Email),
		Data:    user,
	}
	//-
	_ = app.WriteJSON(w, http.StatusAccepted, payload)
}

func (app *Config) logRequest(name, data string) error {
	//-
	var entry struct {
		Name string `json:"name"`
		Data string `json:"data"`
	}
	entry.Name = name
	entry.Data = data

	//-
	jsonData, _ := json.MarshalIndent(entry, "", "\t")

	//-
	loggerServiceURL := "http://" + logServiceUrl + ":13000" + "/log"
	request, err := http.NewRequest(http.MethodPost, loggerServiceURL, bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println("ERROR to send from AUTH-SERVICE to LOGGER-SERVICE data ")
		return err
	}

	//-
	client := &http.Client{}
	_, err = client.Do(request)
	if err != nil {
		fmt.Println("auth/handler: ERROR Auth-service DO request")
		return err
	}

	//- return
	return nil
}
