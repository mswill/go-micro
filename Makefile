broker  = go-micro_broker-service
mail    = go-micro_mail-service
front   = go-micro_front-service
auth    = go-micro_auth-service
logger  = go-micro_logger-service
#---
makeDev = docker-compose -f docker-compose-development.yaml up --build
makeProd = docker-compose -f docker-compose-production.yaml up --build

dev:
	$(info  " --------------------------------  Starting DEVELOPMENT docker images -------------------------------- ")
	$(info \\\\\\)
	$(makeDev)


prod:
	$(info  " ---- Starting PRODUCTION docker images ...  ---- ")
	$(makeProd)

down:
	$(info  " ---- Stopping  docker images... ---- ")
	docker-compose -f docker-compose-development.yaml down

cp:
	$(info  " ---- Clean docker containers prune ---- ")
	docker container prune

removeContainers:
	docker rmi $(broker) $(mail) $(front) $(auth) $(logger)

kill: down removeContainers


# swarm
swarmup:
	docker stack deploy -c swarm.yaml myapp

swarmdown:
	docker stack rm myapp
