FROM golang:1.18.0-alpine as builder
RUN echo '----------------- LISTENER-SERVICE -----------------'

# deploy
RUN mkdir /app
WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY ./ ./

RUN CGO_ENABLED=0 GOOS=linux go build -o listenerApp ./cmd/api
RUN chmod  +x /app/listenerApp

# build
FROM alpine:3.11.3
RUN mkdir /app
COPY --from=builder /app/listenerApp /app
CMD ["/app/listenerApp"]
