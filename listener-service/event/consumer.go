package event

import (
	"bytes"
	"encoding/json"
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"net/http"
)

//var loggerServiceHost = os.Getenv("LOGGER_SERVICE")
var loggerServiceHost = "logger-service"

type Payload struct {
	Name string `json:"name"`
	Data string `json:"data"`
}
type Consumer struct {
	conn      *amqp.Connection
	queueName string
}

func NewConsumer(conn *amqp.Connection) (*Consumer, error) {
	//-
	consumer := &Consumer{conn: conn}

	//-
	err := consumer.setup()
	if err != nil {
		log.Fatal(err)
		return &Consumer{}, err
	}

	//- return
	return consumer, nil
}

func (c Consumer) setup() error {
	channel, err := c.conn.Channel()
	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	//-
	return DeclareExchange(channel)
}

func (c Consumer) Listen(topics []string) error {
	//-
	ch, err := c.conn.Channel()
	if err != nil {
		fmt.Println(err)
		return err
	}
	defer ch.Close()

	//-
	q, err := declareRandomQueue(ch)
	if err != nil {
		fmt.Println(err)
		return err
	}

	//-
	for _, s := range topics {
		err := ch.QueueBind(q.Name, s, "logs_topic", false, nil)
		if err != nil {
			fmt.Println(err)
			return err
		}
	}
	//-
	messages, err := ch.Consume(q.Name, "", true, false, false, false, nil)
	if err != nil {
		fmt.Println(err)
		return err
	}

	//-
	forever := make(chan bool)
	go func() {
		for d := range messages {
			var payload Payload
			_ = json.Unmarshal(d.Body, &payload)

			go handlePayload(payload)
		}
	}()
	fmt.Printf("Waiting for massage on exchange  [Exchange, Queue] [logs_topic, %s\n] ", q.Name)
	<-forever

	//-
	return nil
}

func handlePayload(payload Payload) {
	switch payload.Name {
	case "log", "event":
		//log whatever we get
		err := logEvent(payload)
		if err != nil {
			fmt.Println(err)
			return
		}

	case "auth":
	//
	default:
		err := logEvent(payload)
		if err != nil {
			fmt.Println(err)
			return
		}
	}
}

func logEvent(entry Payload) error {
	//-
	fmt.Println("-------------- LOG ---------------")
	fmt.Println(entry)
	//-
	jsonData, _ := json.MarshalIndent(entry, "", "\t")

	//-
	loggerServiceURL := "http://" + loggerServiceHost + ":13000" + "/log"
	request, err := http.NewRequest(http.MethodPost, loggerServiceURL, bytes.NewBuffer(jsonData))
	if err != nil {
		fmt.Println(err)
		return err
	}

	//-
	request.Header.Set("Content-Type", "application/json")

	//-
	client := &http.Client{}
	res, err := client.Do(request)
	defer res.Body.Close()
	if err != nil {
		fmt.Println(err)
		return err
	}

	//-
	if res.StatusCode != http.StatusAccepted {
		fmt.Println(err)
		return err
	}

	//- return
	return nil
}
