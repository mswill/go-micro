package main

import (
	"fmt"
	amqp "github.com/rabbitmq/amqp091-go"
	ev "gitlab.com/mswill/listener-service/event"
	"log"
	"math"
	"os"
	"strings"
	"time"
)

func main() {
	//-
	// connect rabbitmq
	connect, err := rabbitMQConnect()
	if err != nil {
		log.Fatal(err)
	}
	defer connect.Close()

	//-
	// start listening for messages
	fmt.Println("Listening for and consuming RabbitMQ messages...")

	//-
	// create consumer
	counsumer, err := ev.NewConsumer(connect)
	if err != nil {
		panic(err)
	}

	//-
	// watch the queue and consume events
	err = counsumer.Listen([]string{"log.INFO", "log.WARNING", "log.ERROR"})
	if err != nil {
		fmt.Println(err)
	}
}

func rabbitMQConnect() (*amqp.Connection, error) {

	//-
	var counts int64
	var backOff = 1 * time.Second
	var connection *amqp.Connection

	//-
	//- don't continue until rabbit is ready
	for {
		conn, err := amqp.Dial("amqp://quest:quest@rabbitmq")
		if err != nil {
			fmt.Println("ERROR: RabbitMQ not yet ready...")
			counts++
		} else {
			connection = conn
			fmt.Println("Connected to RABBITMQ")
			fmt.Printf("%s\n", strings.Repeat("-", 25))
			fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting Listener-Service on port: "+os.Getenv("LISTENER_SERVER_PORT"), strings.Repeat("-", 10))
			fmt.Printf("%s\n", strings.Repeat("-", 25))
			break
		}

		//-
		if counts > 5 {
			fmt.Println("Counts great than 5 ", err)
			return nil, err
		}

		//-
		backOff = time.Duration(math.Pow(float64(counts), 2)) * time.Second
		fmt.Printf("backing off ...\n")
		time.Sleep(backOff)
		continue
	}

	//- return
	return connection, nil
}
