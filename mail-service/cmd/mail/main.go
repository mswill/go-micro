package main

import (
	"fmt"
	"gitlab.com/mswill/mail-service/mailer"
	"gitlab.com/mswill/mail-service/routes"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
)

var MAIL_SERVICE_PORT = os.Getenv("MAIL_SERVICE_PORT")

func main() {

	//-
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("%s %s %s\n", strings.Repeat("-", 10), "Starting Mail-Service on port: "+MAIL_SERVICE_PORT, strings.Repeat("-", 10))
	fmt.Printf("%s\n", strings.Repeat("-", 25))

	//-
	app := routes.Config{
		Mailer: createEmail(),
	}

	//-
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", MAIL_SERVICE_PORT),
		Handler: app.Routes(),
	}

	err := srv.ListenAndServe()
	if err != nil {
		log.Panicln(err)
		return
	}
}

func createEmail() mailer.Mail {
	//-

	port, _ := strconv.Atoi(os.Getenv("MAIL_PORT"))
	m := mailer.Mail{
		Domain:      os.Getenv("MAIL_DOMAIN"),
		Host:        os.Getenv("MAIL_HOST"),
		Port:        port,
		UserName:    os.Getenv("MAIL_USERNAME"),
		Password:    os.Getenv("MAIL_PASSWORD"),
		Encrypt:     os.Getenv("MAIL_ENCRYPTION"),
		FromName:    os.Getenv("MAIL_FROM_NAME"),
		FromAddress: os.Getenv("MAIL_FROM_ADDRESS"),
	}

	fmt.Printf("%s\n", strings.Repeat("-", 25))
	fmt.Printf("MAIL_PORT: %s\n", os.Getenv("MAIL_PORT"))
	fmt.Printf("MAIL_DOMAIN: %s\n", os.Getenv("MAIL_DOMAIN"))
	fmt.Printf("MAIL_HOST: %s\n", os.Getenv("MAIL_HOST"))
	fmt.Printf("MAIL_USERNAME: %s\n", os.Getenv("MAIL_USERNAME"))
	fmt.Printf("MAIL_PASSWORD: %s\n", os.Getenv("MAIL_PASSWORD"))
	fmt.Printf("MAIL_ENCRYPTION: %s\n", os.Getenv("MAIL_ENCRYPTION"))
	fmt.Printf("MAIL_FROM_NAME: %s\n", os.Getenv("MAIL_FROM_NAME"))
	fmt.Printf("MAIL_FROM_ADDRESS: %s\n", os.Getenv("MAIL_FROM_ADDRESS"))
	fmt.Printf("%s\n", strings.Repeat("-", 25))
	return m
}
