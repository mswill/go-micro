package routes

import (
	"gitlab.com/mswill/mail-service/mailer"
	"net/http"
)

func (app *Config) SendMail(writer http.ResponseWriter, request *http.Request) {
	//-
	type mailMessage struct {
		From    string `json:"from"`
		To      string `json:"to"`
		Subject string `json:"subject"`
		Message string `json:"message"`
	}
	//-
	var requestPayload mailMessage
	err := app.ReadJSON(writer, request, &requestPayload)
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	msg := mailer.Message{
		From:    requestPayload.From,
		To:      requestPayload.To,
		Subject: requestPayload.Subject,
		Data:    requestPayload.Message,
	}
	//-
	err = app.Mailer.SendSMTPMessage(msg)
	if err != nil {
		_ = app.ErrorJSON(writer, err)
		return
	}

	//-
	payload := jsonResponse{
		Error:   false,
		Message: "send to " + requestPayload.To,
	}
	//-
	_ = app.WriteJSON(writer, http.StatusAccepted, payload)
}
