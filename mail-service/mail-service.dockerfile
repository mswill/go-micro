FROM golang:1.18.0-alpine as builder
RUN echo '----------------- MAIL-SERVICE -----------------'

# deploy
RUN mkdir /app
WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY ./ ./
RUN CGO_ENABLED=0 GOOS=linux go build -o mailApp ./cmd/mail

RUN chmod  +x /app/mailApp

# build
FROM alpine:3.11.3
RUN mkdir /app
COPY --from=builder /app/mailApp /app
CMD ["/app/mailApp"]
