package mailer

import (
	"bytes"
	"fmt"
	"github.com/vanng822/go-premailer/premailer"
	mail "github.com/xhit/go-simple-mail/v2"
	"html/template"
	"time"
)

type Mail struct {
	Domain      string
	Host        string
	UserName    string
	Password    string
	Encrypt     string
	FromAddress string
	FromName    string
	Port        int
}

type Message struct {
	From        string
	To          string
	FromName    string
	Subject     string
	Attachments []string
	DataMap     map[string]any
	Data        any
}

func (m *Mail) SendSMTPMessage(msg Message) error {

	//-
	// set FROM default
	if msg.From == "" {
		msg.From = m.FromAddress
	}
	// set FROM NAME default
	if msg.FromName == "" {
		msg.FromName = m.FromName
	}

	//-
	data := map[string]any{
		"message": msg.Data,
	}
	msg.DataMap = data

	//-
	formattedMessage, err := m.buildHTMLMessage(msg)
	if err != nil {
		fmt.Println("ERROR: buildHTMLMessage")
		return err
	}

	//-
	plainMessage, err := m.buildPlainTextMassage(msg)
	if err != nil {
		fmt.Println("ERROR: buildPlainTextMassage")
		return err
	}

	//-
	// Create SERVER
	server := mail.NewSMTPClient()
	server.Host = m.Host
	server.Port = m.Port
	server.Username = m.UserName
	server.Password = m.Password
	server.Encryption = m.getEncryption(m.Encrypt)
	server.KeepAlive = false
	server.ConnectTimeout = 10 * time.Second
	server.SendTimeout = 10 * time.Second

	//-
	// Connect to CLIENT
	smtpClient, err := server.Connect()
	if err != nil {
		fmt.Println("ERROR: server.Connect")
		return err
	}

	//-
	email := mail.NewMSG()
	email.SetFrom(msg.From).
		AddTo(msg.To).
		SetSubject(msg.Subject)

	//-
	email.SetBody(mail.TextPlain, plainMessage)
	email.AddAlternative(mail.TextHTML, formattedMessage)

	//-
	if len(msg.Attachments) > 0 {
		for _, x := range msg.Attachments {
			email.AddAttachment(x)
		}
	}
	//-
	err = email.Send(smtpClient)
	if err != nil {
		fmt.Println("ERROR: email.Send")
		return err
	}

	return nil
}

func (m *Mail) buildPlainTextMassage(msg Message) (string, error) {

	templateToRender := "./templates/mail.plain.gohtml"

	t, err := template.New("email-plain").ParseFiles(templateToRender)
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	if err = t.ExecuteTemplate(&tpl, "body", msg.DataMap); err != nil {
		return "", err
	}

	plainMessage := tpl.String()

	return plainMessage, nil
}

func (m *Mail) buildHTMLMessage(msg Message) (string, error) {
	templateToRender := "./templates/mail.html.gohtml"

	t, err := template.New("email-html").ParseFiles(templateToRender)
	if err != nil {
		return "", err
	}

	var tpl bytes.Buffer
	if err = t.ExecuteTemplate(&tpl, "body", msg.DataMap); err != nil {
		return "", err
	}

	formattedMessage := tpl.String()
	formattedMessage, err = m.inlineCSS(formattedMessage)
	if err != nil {
		return "", err
	}

	return formattedMessage, nil
}

func (m *Mail) inlineCSS(s string) (string, error) {
	options := premailer.Options{
		RemoveClasses:     false,
		CssToAttributes:   false,
		KeepBangImportant: true,
	}

	prem, err := premailer.NewPremailerFromString(s, &options)
	if err != nil {
		return "", err
	}

	html, err := prem.Transform()
	if err != nil {
		return "", err
	}

	return html, nil
}

func (m *Mail) getEncryption(s string) mail.Encryption {
	//-
	switch s {
	case "tls":
		return mail.EncryptionSTARTTLS
	case "ssl":
		return mail.EncryptionSSLTLS
	case "none":
		return mail.EncryptionNone
	default:
		return mail.EncryptionSTARTTLS
	}
}
